<?php
/**
 * @file
 * mediation_roles.features.wysiwyg_template.inc
 */

/**
 * Implementation of hook_wysiwyg_template_default_templates().
 */
function mediation_roles_wysiwyg_template_default_templates() {
  $templates = array();
  $templates['dita_task'] = array(
    'title' => 'DITA task',
    'description' => 'DITA task',
    'body' => '<p class="l01">This is a task</p>',
    'name' => 'dita_task',
  );
  return $templates;
}
