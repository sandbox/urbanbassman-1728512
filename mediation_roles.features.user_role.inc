<?php
/**
 * @file
 * mediation_roles.features.user_role.inc
 */

/**
 * Implementation of hook_user_default_roles().
 */
function mediation_roles_user_default_roles() {
  $roles = array();

  // Exported role: RFP editor
  $roles['RFP editor'] = array(
    'name' => 'RFP editor',
    'weight' => '4',
  );

  // Exported role: administrator
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '5',
  );

  // Exported role: content manager
  $roles['content manager'] = array(
    'name' => 'content manager',
    'weight' => '2',
  );

  // Exported role: dita editor
  $roles['dita editor'] = array(
    'name' => 'dita editor',
    'weight' => '3',
  );

  return $roles;
}
