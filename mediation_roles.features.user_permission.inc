<?php
/**
 * @file
 * mediation_roles.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function mediation_roles_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_header
  $permissions['create field_header'] = array(
    'name' => 'create field_header',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_mini_toc
  $permissions['create field_mini_toc'] = array(
    'name' => 'create field_mini_toc',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_next
  $permissions['create field_next'] = array(
    'name' => 'create field_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_prev_next
  $permissions['create field_prev_next'] = array(
    'name' => 'create field_prev_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_previous
  $permissions['create field_previous'] = array(
    'name' => 'create field_previous',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create topic content
  $permissions['create topic content'] = array(
    'name' => 'create topic content',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'administrator',
      2 => 'content manager',
      3 => 'dita editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any topic content
  $permissions['delete any topic content'] = array(
    'name' => 'delete any topic content',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'administrator',
      2 => 'content manager',
      3 => 'dita editor',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own topic content
  $permissions['delete own topic content'] = array(
    'name' => 'delete own topic content',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'administrator',
      2 => 'content manager',
      3 => 'dita editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any topic content
  $permissions['edit any topic content'] = array(
    'name' => 'edit any topic content',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'administrator',
      2 => 'content manager',
      3 => 'dita editor',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_header
  $permissions['edit field_header'] = array(
    'name' => 'edit field_header',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_mini_toc
  $permissions['edit field_mini_toc'] = array(
    'name' => 'edit field_mini_toc',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_next
  $permissions['edit field_next'] = array(
    'name' => 'edit field_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_prev_next
  $permissions['edit field_prev_next'] = array(
    'name' => 'edit field_prev_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_previous
  $permissions['edit field_previous'] = array(
    'name' => 'edit field_previous',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_header
  $permissions['edit own field_header'] = array(
    'name' => 'edit own field_header',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_mini_toc
  $permissions['edit own field_mini_toc'] = array(
    'name' => 'edit own field_mini_toc',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_next
  $permissions['edit own field_next'] = array(
    'name' => 'edit own field_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_prev_next
  $permissions['edit own field_prev_next'] = array(
    'name' => 'edit own field_prev_next',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_previous
  $permissions['edit own field_previous'] = array(
    'name' => 'edit own field_previous',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own topic content
  $permissions['edit own topic content'] = array(
    'name' => 'edit own topic content',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'administrator',
      2 => 'content manager',
      3 => 'dita editor',
    ),
    'module' => 'node',
  );

  // Exported permission: use text format content_editor
  $permissions['use text format content_editor'] = array(
    'name' => 'use text format content_editor',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'content manager',
      2 => 'dita editor',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format dita
  $permissions['use text format dita'] = array(
    'name' => 'use text format dita',
    'roles' => array(
      0 => 'content manager',
      1 => 'dita editor',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format filtered_html
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format full_html
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format rfp
  $permissions['use text format rfp'] = array(
    'name' => 'use text format rfp',
    'roles' => array(
      0 => 'RFP editor',
      1 => 'content manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: view field_header
  $permissions['view field_header'] = array(
    'name' => 'view field_header',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_mini_toc
  $permissions['view field_mini_toc'] = array(
    'name' => 'view field_mini_toc',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_next
  $permissions['view field_next'] = array(
    'name' => 'view field_next',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_prev_next
  $permissions['view field_prev_next'] = array(
    'name' => 'view field_prev_next',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_previous
  $permissions['view field_previous'] = array(
    'name' => 'view field_previous',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_header
  $permissions['view own field_header'] = array(
    'name' => 'view own field_header',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_mini_toc
  $permissions['view own field_mini_toc'] = array(
    'name' => 'view own field_mini_toc',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_next
  $permissions['view own field_next'] = array(
    'name' => 'view own field_next',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_prev_next
  $permissions['view own field_prev_next'] = array(
    'name' => 'view own field_prev_next',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_previous
  $permissions['view own field_previous'] = array(
    'name' => 'view own field_previous',
    'roles' => array(
      0 => 'administrator',
      1 => 'content manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
